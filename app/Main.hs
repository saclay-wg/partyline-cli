{-# LANGUAGE RecordWildCards #-}
module Main (main) where

import           Data.Aeson.Encode.Pretty
import           Options.Applicative
import           PartyLine.Utils

data Argument = Argument { userModel  :: Bool
                         , eventModel :: Bool
                         , postModel  :: Bool
                         , groupModel :: Bool
                         }

main :: IO ()
main = execParser opts >>= runWithOptions
  where
    parser :: Parser Argument
    parser = Argument <$> switch (short 'u' <>
                                  long "user" <>
                                  help "Display an example User JSON object"
                                 )
                      <*> switch (short 'e' <>
                                  long "event" <>
                                  help "Display an example Event JSON object"
                                 )
                      <*> switch (short 'p' <>
                                  long "post" <>
                                  help "Display an example Post JSON object"
                                 )
                      <*> switch (short 'g' <>
                                  long "group" <>
                                  help "Display an example Group JSON object"
                                 )
    opts = info (parser <**> helper) (fullDesc
                                     <> progDesc "Helper program for the PartyLine standard"
                                     <> header "PartyLine — © 2019 Saclay Working Group <https://saclay-wg.gitlab.io>"
                                     )

runWithOptions :: Argument -> IO ()
runWithOptions Argument{..} = do
  user  <- encodePretty <$> mkUser
  post  <- encodePretty <$> mkPost
  event <- encodePretty <$> mkEvent
  group <- encodePretty <$> mkGroup
  let a = bool "" user userModel
  let b = bool "" event eventModel
  let c = bool "" post postModel
  let d = bool "" group groupModel
  let result = filter (/= "") [a,b,c,d]
  mapM_ putLBSLn result

module PartyLine.Utils where

import           Data.Time.Clock       (getCurrentTime)
import           Data.Time.Convenience
import           Data.Time.ISO8601     (formatISO8601Javascript)
import qualified Data.UUID             as UUID
import           Data.UUID.V4          (nextRandom)
import           PartyLine.Schema


mkGroup :: IO Group
mkGroup = do
  groupUUID <- UUID.toText <$> nextRandom
  pure $ Group { name = "Rezel"
        , photo = "https://assets.example/photo/" <> groupUUID
        , email = "spam@rezel.net"
        , uuid = groupUUID
        }


mkEvent :: IO Event
mkEvent = do
  eventUUID <- UUID.toText <$> nextRandom
  startTime <- toText . formatISO8601Javascript <$> getCurrentTime
  endTime   <- toText . formatISO8601Javascript <$> timeFor 2 Hour FromNow
  pure $ Event { author = "Þórdís.reykfjörð.gylfadóttir@bifrost.is"
        , title = "Soirée ramassage de déchets"
        , startTime = startTime
        , endTime = endTime
        , description = "Lorem ipsum\nBlablabla…"
        , place = "Campus"
        , uuid = eventUUID
        }

mkUser :: IO User
mkUser = do
  userUUID <- UUID.toText <$> nextRandom
  pure $ User { displayName = "Raúl Rahmani"
              , email    = "r.rahmani@example.com"
              , photo    = "https://assets.example.com/photo/" <> userUUID
              , uuid     = userUUID
              }

mkPost :: IO Post
mkPost = do
  event <- mkEvent
  postUUID <- UUID.toText <$> nextRandom
  parentUUID <- UUID.toText <$> nextRandom
  now      <- toText . formatISO8601Javascript <$> getCurrentTime
  pure $ Post { uuid = postUUID
              , title = "Soirée ramassage de déchets"
              , author = "Þórdís Reykfjörð Gylfadóttir"
              , description = "Nous irons tous⋅tes ramasser les déchets traînant sur nos campus respectifs !"
              , attachments = []
              , createdAt = now
              , updatedAt = now
              , parent = Just $ "https://api.example.com/post/" <> parentUUID
              }

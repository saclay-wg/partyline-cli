module PartyLine.Schema where

import           Data.Aeson

type URL = Text

data Post = Post { uuid        :: Text
                 , title       :: Text
                 , author      :: Text
                 , description :: Text
                 , attachments :: [URL]
                 , createdAt   :: Text
                 , updatedAt   :: Text
                 , parent      :: Maybe URL
                 } deriving (Show, Generic)

data User = User { displayName :: Text
                 , email       :: Text
                 , photo       :: URL
                 , uuid        :: Text
                 } deriving (Show, Generic)

data Group = Group { name  :: Text
                   , photo :: URL
                   , email :: Text
                   , uuid  :: Text
                   } deriving (Show, Generic)

data Event = Event { author      :: Text
                   , title       :: Text
                   , startTime   :: Text
                   , endTime     :: Text
                   , description :: Text
                   , place       :: Text
                   , uuid        :: Text
                   } deriving (Show, Generic)

data Parent = Parent { parentEvent :: Maybe Event
                     , parentPost  :: Maybe Post
                     } deriving (Show, Generic)

instance FromJSON Post
instance ToJSON Post

instance FromJSON User
instance ToJSON User

instance FromJSON Group
instance ToJSON Group

instance FromJSON Event
instance ToJSON Event

instance FromJSON Parent
instance ToJSON Parent

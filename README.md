# partyline-cli

This utility prints an example JSON object for the selected model (User, Event, Post, Group)

## Usage

1. To show the help message:

```bash
$ partyline -h
```

2. To show an example User model

```bash
$ partyline -u
```

## Install

Either download the latest binary [from Gitlab](https://gitlab.com/saclay-wg/partyline-cli/raw/master/partyline)
Or:

1. Install [Haskell Stack](https://docs.haskellstack.org/en/stable/README/) with 

`$ curl -sSL https://get.haskellstack.org/ | sh`

2. Run this command

`$ stack install`

And `partyline` should be installed in your `~/.local/bin` directory
